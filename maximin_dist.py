import numpy as np
from scipy.optimize import fmin_tnc
import scipy.spatial as scp

#find the point that maximizes the minimum distance to all already evaluated points
def maximin_dist(data):
	allpoints = data.S#all points that have been evaluated so far, feasible, infeasible, crash points
	fbest = np.inf
	myfun2 = lambda x: shortest_distance(x,allpoints) #the function to maximize (minimize -f(x))
	bound_list=[] #list of upper and lower bounds
	lower=data.xlow
	upper=data.xup

	for ii in range(data.dim):
		bound_pair = (np.ravel(np.asarray(lower))[ii], np.ravel(np.asarray(upper))[ii])
		bound_list.append(bound_pair)
	
	for ii in range(data.dim): #do several trials to find the point that maximizes the minimum distance
		x0 = np.asarray(lower) + np.asarray(upper-lower) * np.asarray(np.random.rand(1,data.dim)) #random starting point
		xout, out1,out2 = fmin_tnc(myfun2, x0, bounds=bound_list, approx_grad = True, disp = 0)
		fout = myfun2(xout)
		if fout < fbest:
			xnew = xout
			fbest = fout

	target_mat = np.kron(np.ones((allpoints.shape[0],1)),xnew) - allpoints #distance of new point to all previously evaluated points
	while (np.sqrt(np.sum(np.power(target_mat,2),axis=1)) < data.tolerance).any(): #if we are too close to a previously evaluated point, select a new point at random
		xnew = data.xlow + (data.xup-data.xlow)*np.asarray(np.random.rand(1,dim))
		target_mat = np.kron(np.ones((allpoints.shape[0],1)),xnew) - allpoints
	return xnew

def shortest_distance(x,S): #find minimum distance of x to S, maximize this minimum distance by minimizing -f(x)
	a = scp.distance.cdist(np.asmatrix(x), S)
	obj_val=-np.amin(a)
	return obj_val 
