
#----------------********************************--------------------------
import numpy as np
import scipy.spatial as scp
from phi import phi
from util import myException

def rbf_pred(x, S, lam,gam, flag):
    #should be the same type (cubic +linear tail) as the one trained
    polynom = 'linear'
    n_points = x.shape[0] # determine number of candidate points
    dim= S.shape[1]
    # compute pairwise distances between candidates and already sampled points
    Normvalue = np.transpose(scp.distance.cdist(x, S))
    # compute radial basis function value for distances
    U_Y = phi(Normvalue, flag)

    
    # determine the polynomial tail (depending on rbf model)
    if polynom == 'none':
        PolyPart = np.zeros((n_points, 1))
    elif polynom == 'constant':
        PolyPart = gam * np.ones((n_points, 1))
    elif polynom == 'linear':
        A= np.asmatrix(np.concatenate((np.ones((n_points, 1)), x), axis = 1))
        PolyPart = A*gam
    elif polynom == 'quadratic':
        temp = np.concatenate(np.concatenate((np.ones((n_points, 1)), x), axis = 1), \
                np.zeros((n_points, (dim * (dim + 1)) / 2)), axis = 1)
    else:
        raise myException('Error: Invalid polynomial tail.')

    RBFvalue = np.asmatrix(U_Y).T * np.asmatrix(lam) + PolyPart

    return RBFvalue

