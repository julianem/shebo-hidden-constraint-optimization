
import numpy as np
import os, sys
import subprocess
import numpy as np
import shutil
from objective import objective

def objectivewrap_for_nomad():
	#these lines are required
	filename = sys.argv[-1] #input file is txt file with a vector of numbers separated by spaces
	my_new_input = open(filename,'r') #read data point
	datap = my_new_input.read()
	my_new_input.close()
	x = np.asmatrix(datap)#put data point into np matrix
	
	seed= x[0,0] #first number is seed - needed to match NOMAD input and output files
	tag = x[0,1] #second number is tag -- need these two numbers for writing to output file
	
	xv=x[0,2:] #variable vector is everythong in input file starting from third number
	dim = xv.shape[1] #problem dimension
	

	
	#call to your expensive objective function happens here (redefine as needed): 
	x_sc = np.asmatrix(xv)
	y=objective(xv)

	
	#the following lines are required for NOMAD to find the output -- use your respective directory
	#determine correct input file name nomad.x.y.z.input to determine name for correct output file name nomad.x.y.z.output
	for file in os.listdir("/home/julianem/hidden_constraints/tmp"):
    		if file.endswith(".input"):
			outstr=file[0:len(file)-5]
	sty=("/home/julianem/hidden_constraints/tmp/",outstr,"output")
	outstr=''.join(sty) #string for output file name
	my_new_res = open(outstr,'w')#write function value into output file
	my_new_res.write(str(y))
	my_new_res.close()

	return y
	

if __name__ == "__main__":
   	objectivewrap_for_nomad()

