#symmetric latin hypercube

import numpy as np

def slhd(d, m):
    
    delta = (1.0 / m) * np.ones(d);
    X = np.zeros([m, d])
    for j in range(d):
        for i in range(m):
            X[i, j] = ((2.0 * (i + 1) - 1) / 2.0) * delta[j]
    P = np.zeros([m, d], dtype = int);   

    P[:,0] = np.arange(m)
    if m % 2 == 0:
        k = m / 2
    else:
        k = (m - 1) / 2
        P[k, :] = (k + 1) * np.ones((1, d))

    for j in range(1, d):
        P[0:k, j] = np.random.permutation(np.arange(k))
        for i in range(k):
            if np.random.random() < 0.5:
                P[m - 1 - i, j] = m - 1 - P[i, j]
            else:
                P[m - 1 - i, j] = P[i, j]
                P[i, j] = m - 1 - P[i, j]
    InitialPoints = np.zeros([m, d])
    for j in range(d):
        for i in range(m):
            InitialPoints[i, j] = X[P[i, j], j]
    return InitialPoints


