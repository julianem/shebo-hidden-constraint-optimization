import numpy as np

def objective(xv):

	#default parameter bounds are [0,1] -- if you have different ones, scale back to true range here
	#true_lb=array your lower bounds
	#true_ub=arrray your upper bounds
	#x_sc = true_lb+(true_ub-true_lb)*np.ravel(xv)
	x_sc=np.asmatrix(xv)
	if x_sc[0,0]>0.9:
		y= np.nan
	else: 
		y= np.sum(np.power(x_sc,2))

	save_str2=""
	for ii in range(x_sc.shape[1]):
		save_str2 = ''.join((save_str2,str(x_sc[0,ii])))
		save_str2 = ''.join((save_str2,','))
	my_in = open('/home/julianem/hidden_constraints/allsamples_x.txt','a')
	my_in.write(save_str2)
	my_in.write(str(y))
	my_in.write("\n")
	my_in.close()

	return y
