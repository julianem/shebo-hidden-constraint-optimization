# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The code allows you to do optimization of computationally expensive simulations that may fail to evaluate
* Version 1.0, tested on linux

### How do I get set up? ###

* Understand the copyright and licensing information
* Read the paper: https://pubsonline.informs.org/doi/10.1287/ijoc.2018.0864
* Rad the manual
* Download the files that are in this repo
* Requires python 2.7, requires NOMAD version 3.9.1 (https://www.gerad.ca/nomad/)
* Install NOMAD as instructed in NOMAD user manual
* Adjust code lines in write_nomad_prms.py and objectivewrap_for_nomad.py (set to your own directories where python codes and nomad are located), but keep file names the same 
* Make sure you made a tmp folder
* define your objective function call in objective.py and adjust directory to point to your local folders
* python hico-optimizer.py should run the program from the command line




### Who do I talk to? ###
* Juliane Mueller, JulianeMueller@lbl.gov
