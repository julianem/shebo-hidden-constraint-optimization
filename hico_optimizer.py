import sys
import os
import importlib
import numpy as np
import cPickle as p
import random
import copy
from util import *
from slhd import slhd #uses symmetric latin hypercube, but can also use other design strategy
from maximin_dist import maximin_dist
from rbf import rbf
from sampling import local_search, global_search, cycle_search
from nomad_init import nomad_init
from objectivewrap_for_nomad import objectivewrap_for_nomad
from objective import objective



def hico_optimizer(trial_nr):#

    	np.random.seed([trial_nr])
    	random.seed(trial_nr)

    	data = Data()
	data.objfunction= objective#uqb_obj
	data.dim = 5 #for test problem	
	data.xlow = np.zeros(data.dim) #optimizer works in [0,1] -- if you have different bounds, supply a rescaling in the objective.py file
    	data.xup = np.ones(data.dim) #upper bounds are always 1, if different, rescale as for xlow in objective.py
	data.testcase = trial_nr#1#problem_nr
	 
	data.maxeval = 50 *data.dim #define the maximum number of allowable function evaluations
	data.tolerance = 0.001# distance that two sample points have to be away from each others at least (assuming that the difference between two sample points that are tol away form each others does not influence the results significantly)
	data.num_startp = 5*(data.dim+1) #number of initial evaluation points (if code is known to fail very often, this number should be somewhat large)
	data.Ncands = 500*data.dim
	data.std = 0.02 #perturbation for creating candidate points
	data.eval_th = 0.25 #;%=0.75;% 0.95;% t; %0.1;%10e-8; %PARAMETER
	data.w_cyc = np.array([1, 0.95, 0.85, 0.75, 0.50, 0.35, 0.25, 0.1, 0])
	data.flag_ev = 'cubic'
	data.flag_01 = 'linear'
	data.nomad_evalsmult = 4 #number of local search evaluations (4 times dimension))
	data.S =[]# matrix of sample points
	data.Y = None   #function values

	init_p=slhd(data.dim, data.num_startp) #initial experimental design - replace with own design if needed	
	init_v = np.zeros((data.num_startp,1))
	xev = [] #stores successful points
	yev = [] #stores successful evaluations
	x_nev = [] #stores unsuccessful points
	f_01 = []
	
	#go through start points and evaluate objective
	for ii in range(data.num_startp):
		v = data.objfunction(init_p[ii,:])		
		init_v[ii] = v   
		if not(np.isnan(v)): #successful evaluation
			if len(xev) == 0: #this is the first evaluated point
				xev = np.asmatrix(init_p[ii,:])
				yev = np.asmatrix(v)
			else:
				xev = np.concatenate((xev, np.asmatrix(init_p[ii,:])), axis = 0) #c
				yev = np.concatenate((yev, np.asmatrix(v)),axis=0)	
				
			if len(f_01) == 0:
				f_01 = np.asmatrix(1)
			else:
				f_01 = np.concatenate((f_01, np.asmatrix(1)),axis = 0)	
		else:#unsuccessful evaluation
			if len(x_nev) == 0:
				x_nev = np.asmatrix(init_p[ii,:])
			else:
				x_nev = np.concatenate((x_nev, np.asmatrix(init_p[ii,:])), axis = 0) #c
			if len(f_01) == 0:
				f_01 = np.asmatrix(0)
			else:
				f_01 = np.concatenate((f_01, np.asmatrix(0)),axis = 0)
		if len(data.S) == 0:
			data.S = np.asmatrix(init_p[ii,:])
			data.Y = np.asmatrix(v)
		else:
			data.S = np.concatenate((data.S, np.asmatrix(init_p[ii,:])),axis=0) 
			data.Y = np.concatenate((data.Y,np.asmatrix(v)),axis=0)

	if not(len(xev) == 0):
		evalb = xev.shape[0]
		A = np.concatenate((xev, np.ones((evalb,1))),axis=1)
		ra = np.linalg.matrix_rank(A)
	else:
		evalb = 0 #current number of successfully evaluated points
		ra = 1 #current rank of the sample site matrix (which is empty, only has column of ones)
	#add points to initial experimental design until RBF condition satisfied

	while (ra<data.dim+1) and (data.S.shape[0] < data.maxeval):
	    print("##########################################################")
	    print("rank: ", ra)
	    print("size S: ", data.S.shape)
	    print("number evaluable: ", evalb, xev.shape)
	    #new point - maximizer of min distance to other points 
	    xbest = maximin_dist(data)
	    fbest = data.objfunction(xbest)
	    if not(np.isnan(fbest)): #successful evaluation
		evalb = evalb+1
		if not(len(xev)==0):
			xev = np.concatenate((xev, np.asmatrix(xbest)), axis = 0) #c
			yev = np.concatenate((yev, np.asmatrix(fbest)),axis=0)
	        	f_01 = np.concatenate((f_01, np.asmatrix(1)),axis = 0)
		else:
			xev = np.asmatrix(xbest)
			yev = np.asmatrix(fbest)
	    else: #unsuccessful evaluation
		f_01 = np.concatenate((f_01, np.asmatrix(0)), axis = 0)
	    data.S = np.concatenate((data.S, np.asmatrix(xbest)),axis = 0)
	    data.Y = np.concatenate((data.Y, np.asmatrix(fbest)), axis = 0)    
	    if not(len(xev)==0): #recompute matrix rank
	    	A = np.concatenate((xev, np.asmatrix(np.ones((evalb,1)))),axis=1)
	    	ra = np.linalg.matrix_rank(A)
	    else:
		ra=1	
	#by the end, we should have either satisfied the RBF condition OR reached the maximum number of allowed evaluations
	n0 = data.S.shape[0]
	if data.S.shape[0]>=data.maxeval: #exhausted function evaluation budget
		print('budget of function evaluations is exhausted; stopping without optimization')		
		solution = data
		return solution

	
	#fit Cubic RBF to {(xev, fev)} data
	[lamb, gam] = rbf(xev,yev,data.flag_ev)
	x_01 = data.S
	#%fit linear RBF to {(x_01, f_01)} which represents the prediction for successful evaluation
	[lamb_01, gam_01] = rbf(x_01,f_01,data.flag_01)#
	
	#find best solution so far
	bestID = np.nanargmin(yev)
	data.fbest = yev[bestID]
	data.xbest = xev[bestID,:]
	
	#%iterative sampling
	#%cycle between local and global searches
	iter_ctr = 0
	cyclestep_runs = 0
	localstep_runs = 0
	globalstep_runs = 0
	cycle_success = 0
	local_success = 0
	global_sucess = 0
	
	while data.S.shape[0] < data.maxeval:
		#data.threshold_dyn = ((np.log(data.S.shape[0]-n0+1))/np.log(data.maxeval-n0)) 
		data.eval_th = ((np.log(data.S.shape[0]-n0+1))/np.log(data.maxeval-n0))#dynamic adjustment of threshold for predicted successful evaluation
		iter_ctr = iter_ctr+1
		print('iteration:', iter_ctr)
                print('nevals=', data.S.shape[0])
    		sample_stage = iter_ctr%(1+len(data.w_cyc)+1) #select sampling method
		print('sample stage:', sample_stage)
    		if sample_stage == 1: #local search
        		localstep_runs = localstep_runs+1
        		sample_strat = 'loc_step' #minimize surrogate surface of f(x) s.t. surrogate surface of evaluability >T
    		elif sample_stage == 0: #global search
        		globalstep_runs = globalstep_runs+1
        		sample_strat = 'global_step' # maximize min distance to already evaluated points
    		else: # %cycling candidate search
        		cyclestep_runs = cyclestep_runs+1
        		sample_strat = 'cycle_step'
    
    		if data.dim <=10: #perturb all variables if dimension is at most 10
        		data.P  = 1;
    		else: #perturb a fraction if dimension is larger than 10
        		data.P = np.random.random(1) 
    
    		#select the new sample point with one of the three methods
    		if sample_strat == 'loc_step':
	        	xnew = local_search(data)
    		elif sample_strat == 'cycle_step':
        		xnew = cycle_search(data,sample_stage)
    		else:
        		xnew = global_search(data)
    
		if len(xnew) == 0: #no new point was found
        		continue #%no evaluation, go to next sample point/strategy    
    		ynew = data.objfunction(xnew)
		print('xnew: ', xnew, ' ynew: ', ynew, ' ybest: ', data.fbest)
		
    		if not(np.isnan(ynew)): #new evaluation was successful
        		if sample_strat == 'loc_step':
           			local_success = local_success+1
       			elif sample_strat == 'cycle_step':
           			cycle_success = cycle_success+1
       			else:
           			global_sucess = global_sucess+1
           
    		if ynew < data.fbest: #new point improved current best solution
       			data.fbest = ynew
       			data.xbest = xnew  
       			nom_search = True #invoke local intensified search with NOMAD
    		else:
        		nom_search = False
    		print('fbest: ', data.fbest)
    		data.S = np.concatenate((data.S,np.asmatrix(xnew)), axis = 0)#update sample site matrix
    		data.Y = np.concatenate((data.Y, np.asmatrix(ynew)), axis = 0) #update function value matrix

		if nom_search: #intensified local search with NOMAD (use nomad3.7.2)
			print">>>>>>>>>>>>>>>>>>>>>>>>>>NOMAD<<<<<<<<<<<<<<<<<<<<<<<<<"
			data_in = copy.copy(data)      		
			data_in = nomad_init(data_in) #initialize nomad -- does some function evaluations
			data = copy.copy(data_in)
		spar = np.isfinite(data.Y)#find the function values that are not NaN
		ind = np.where(spar == True)[0]
		xev = data.S[ind,:] #update of successful evaluation point matrix
    		yev = data.Y[ind] #update of successful function vlaue matrix

		#fit Cubic RBF to successful evaluation pairs {(xev, fev)} data
    		[lamb, gam] = rbf(xev,yev,data.flag_ev);

    		#%set up matrices for fail/no-fail evals
    		fail = np.where(np.isnan(data.Y))[0]
    		xnev = data.S[fail,:]
    		x_01 = data.S
    		f_01 = np.asmatrix(np.ones((data.S.shape[0],1)))
    		f_01[fail,:] = 0
    		#%fit linear RBF to {(x_01, f_01)} (prediction of evaluability with linear RBF)
   	 	[lamb_01, gam_01] = rbf(x_01,f_01,data.flag_01)

	solution = data
	
	return solution



if __name__ == "__main__":
    print 'running hidden constraints code'
    runID = 4 #stochastic algorithm -- set random number seed
    solution = hico_optimizer(runID)
    file_n = "result_test_" 
    set_str = ''.join((file_n, str(runID)))
    s2 = ".data"
    bb = ''.join((set_str, s2))
    f = file(bb, 'w') 
    p.dump(solution, f)
    f.close()
    
