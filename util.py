
#----------------********************************--------------------------
import numpy as np

class myException(Exception):
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg

class DriverWrap:

    def Eval(self, data):
        return self.d.LogLikelihood(data)

    def NumParams(self):
        return self.d.NumParams()

    def NumData(self):
        return self.d.NumData()

    def PriorMean(self):
        return self.d.PriorMean()

    def PriorStd(self):
        return self.d.PriorStd()

    def EnsembleStd(self):
        return self.d.EnsembleStd()

    def GenerateTestMeasurements(self, data):
        return self.d.GenerateTestMeasurements(data)

    def MeasuredData(self):
        return self.d.MeasuredData()

    def MeasuredDataSTD(self):
        return self.d.MeasuredDataSTD()

    def LowerBound(self):
        return self.d.LowerBound()
    def UpperBound(self):
        return self.d.UpperBound()



class Data:
    def __init__(self):
        ## User defined parameters
        self.xlow = None
        self.xup = None
        self.objfunction = None
        self.dim = None
	self.params = None
    def validate(self):
        if self.dim == None:
            raise myException('You must provide the problem dimension.\n')
        if not isinstance(self.dim, int) or self.dim <= 0:
            raise myException('Dimension must be positive integer.')
        if not isinstance(self.xlow, np.matrixlib.defmatrix.matrix) or \
                not isinstance(self.xup, np.matrixlib.defmatrix.matrix) \
                or self.xlow.shape != (1, self.dim) or self.xup.shape != (1, self.dim):
            raise myException('Vector length of lower and upper bounds must equal problem dimension\n')
        #print any(self.xlow[0][i] > 0 for i in range(self.dim))
        #print any(self.xlow[0][i] > self.xup[0][i] for i in range(self.dim))
        comp_list = np.less_equal(self.xlow, self.xup).tolist()[0]
        if any(i == False for i in comp_list):
            raise myException('Lower bounds have to be lower than upper bounds.\n')


class Solution:
    def __init__(self):
        self.BestValues = None
        self.BestPoints = None
        self.NumFuncEval = None
        self.AvgFuncEvalTime = None
        self.FuncVal = None
        self.DMatrix = None
        self.NumberOfRestarts = None
	self.ConstVal = None
