import numpy as np
from phi import phi
import math
import scipy.spatial as scp
from util import myException

def rbf(X, Y, flag):
    #recommend cubic RBF with linear tail
    polynom = 'linear'

    PairwiseDistance = scp.distance.cdist(X, X, 'euclidean')
    nr_evals = X.shape[0]
    dimens = X.shape[1]
    Ftransform = np.copy(np.asarray(Y))
    #if function values have huge range, might transform function values such that highest values are set to median	
    #	medianF = np.median(np.asarray(Y))
    #    Ftransform[Ftransform > medianF] = medianF

    rhs = Ftransform
    
    if flag == 'linear':
        PairwiseDistance = PairwiseDistance
    elif flag == 'cubic':
        PairwiseDistance = PairwiseDistance ** 3
    elif flag == 'thinplate':
        PairwiseDistance = PairwiseDistance ** 2 * math.log(PairwiseDistance + np.finfo(np.double).tiny)

    PHI = PairwiseDistance
    phi0 = phi(0, 'linear') # phi-value where distance of 2 points =0 (diagonal entries)

    if polynom == 'None':
        pdim = 0
        P = np.array([])
    elif polynom == 'constant':
        pdim = 1
    elif polynom == 'linear':
        pdim = dimens + 1
        P = np.concatenate((np.ones((nr_evals, 1)), X), axis = 1)
    elif polynom == 'quadratic':
        pdim = (dimens + 1) * (dimens + 2) / 2
        P = np.concatenate((np.concatenate((np.ones((data.maxeval, 1)), XforMat), axis = 1), np.zeros((data.maxeval, (dimens*(dimens+1))/2))), axis = 1)
    else:
        raise myException('Error: Invalid polynomial tail.')

    PHI_matrix = np.asmatrix(PHI)
    phi0_matrix = np.asmatrix(phi0)
    P_matrix = np.asmatrix(P)

    a_part1 = np.concatenate((PHI_matrix, P_matrix), axis = 1)
    a_part2 = np.concatenate((np.transpose(P_matrix), np.zeros((pdim, pdim))), axis = 1)
    a = np.concatenate((a_part1, a_part2), axis = 0)
    eta = math.sqrt((1e-16) * np.linalg.norm(a, 1) * np.linalg.norm(a, np.inf))
	
    #COMPUTE INVERSE OF LEFT HAND SIDE MATRIX -- need it for objective AND constraints
    A = np.asarray(a + eta * np.eye(nr_evals + pdim))
    Ainv = np.linalg.inv(A)
    #coefficients for objective
    coeffs = np.dot(Ainv, np.asarray(np.concatenate((rhs, np.zeros((pdim, 1))), axis = 0)))
    coeffs_l = np.asmatrix(coeffs[0:nr_evals])
    coeffs_g = np.asmatrix(coeffs[nr_evals:])
    
    return coeffs_l, coeffs_g #return RBF parameters
    
