import numpy as np
from write_nomad_prms import write_nomad_prms
import scipy.spatial as scp
#for python we have to make a second bb definition file that can be called from nomad

def nomad_init(data):
	#define for each test function python code for BB_EVAl
	
	S_sofar = data.S
	x0 = np.ravel(data.S[-1,:]) #last evaluated point is the start point for NOMAD
	maxeval_n = min(data.dim*data.nomad_evalsmult, data.maxeval) #max number of function evals we want to do with NOMAD
	newS, newY = write_nomad_prms(maxeval_n, x0, data)	#write stuff into txt file that NOMAD needs to run the problem
	if not(len(newS)==0): #points were evaluated
		oldS = data.S
		numpoints = newS.shape[0] # determine number of new points
    		# compute pairwise distances between candidates and already sampled points
    		distances = np.transpose(scp.distance.cdist(newS, oldS))
		min_distances = np.asmatrix(np.amin(distances, axis = 0))
		a = np.where(np.ravel(min_distances) > 1e-7)[0] #find points in newS that are sufficiently far away from already evaluated points
		addpoints = newS[a,:]
		addvals = newY.T[a,0]
		data.S = np.concatenate((data.S, addpoints),axis = 0)
		data.Y = np.concatenate((data.Y, addvals),axis = 0)
		spar = np.isfinite(addvals)
		ind = np.where(spar == True)[0]
		xev = addpoints[ind,:]
    		yev = addvals[ind]
		if yev.shape[0]>0:
			minID = np.argmin(np.ravel(yev), axis =0)
			minval = yev[minID,0]
			minpoint=xev[minID,:]
			if minval<data.fbest: #update best point and best function value seen so far
    				data.fbest = minval
    				data.xbest = minpoint
	return data
