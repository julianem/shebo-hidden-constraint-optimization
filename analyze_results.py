import cPickle as p
import numpy as np
from util import *
import matplotlib as mpl
#mpl.use('Qt4Agg')
import matplotlib.pyplot as plt

def analyze_results():
	#sol = p.load(open("result2.data"))
	sol = p.load(open("result_test_4.data"))
	print(sol.Y)
	sortY=np.zeros((sol.Y.shape[0],1))
	sortY[0] = sol.Y[0]

	for ii in range(sol.Y.shape[0]-1):
		if (np.isnan(sortY[ii])) and (np.isnan(sol.Y[ii+1])):
			sortY[ii+1] = sol.Y[ii+1]
		elif (np.isnan(sortY[ii])) and (not(np.isnan(sol.Y[ii+1]))):
			sortY[ii+1] = sol.Y[ii+1]
		elif (not(np.isnan(sortY[ii]))) and (np.isnan(sol.Y[ii+1])):
			sortY[ii+1] =sortY[ii]
		else:
			if sortY[ii]<sol.Y[ii+1]:
				sortY[ii+1] = sortY[ii]
			else:
				sortY[ii+1] = sol.Y[ii+1]
	

	print('number of failed evaluations:', len(np.where(np.isnan(sol.Y))[0]))
	print('best point:', sol.S[np.nanargmin(sol.Y)])
	print('best value:', np.nanmin(sol.Y))
	plt.plot(np.ravel(sortY), linewidth=2.5)
	plt.xlabel('Number of function evaluations')
	plt.ylabel('Objective function value')
	plt.savefig('progress.png')


	return



if __name__=="__main__":
	analyze_results()
