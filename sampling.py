import numpy as np
import scipy.optimize as sco
import scipy.spatial as scp
import copy
import random
import cPickle as p
from rbf import rbf
from rbf_pred import rbf_pred
from maximin_dist import maximin_dist

def local_search(data):
	#local optimization: find minimum or RBF such that predicted evaluability is larger than threshold
	print('local search')
	spar = np.isfinite(data.Y)
	ind = np.where(spar == True)[0]
	xev = data.S[ind,:]
    	yev = data.Y[ind]
	[lamb, gam] = rbf(xev,yev,data.flag_ev);#fit RBF to successful evaluations

	fail = np.where(np.isnan(data.Y))[0]
    	xnev = data.S[fail,:]
    	x_01 = data.S
    	f_01 = np.asmatrix(np.ones((data.S.shape[0],1)))
    	f_01[fail,:] = 0
    	#%fit linear RBF to {(x_01, f_01)}
   	[lamb_01, gam_01] = rbf(x_01,f_01,data.flag_01)	#separate RBF to predict evaluability probability

	#lower and upper bounds for minimization
	bound_list=[]
	lower=np.ravel(data.xlow)
	upper=np.ravel(data.xup)
	#setup local optimization problem	
	for ii in range(data.dim):
		bound_pair = (np.ravel(np.asarray(lower))[ii], np.ravel(np.asarray(upper))[ii])
		bound_list.append(bound_pair)	
	
	fbest = np.inf
	xnew = None
	for ii in range(1): #do optimization for several trials from random starting points
		xr = np.asmatrix(lower + (upper-lower)*np.random.random((1,data.dim)))
		r = sco.fmin_cobyla(f_rbf, xr, c_rbf, args=(xev, lamb, gam, data.flag_ev), consargs=(x_01, lamb_01, gam_01, data.flag_01, data.eval_th, lower, upper),catol = 1e-6)		
		xnew = np.asarray(r[0])

	if (xnew<0).any or (xnew > 1).any: 
		print('#################--------------------Local search 1 failed; use random point!!')		
		xnew = np.random.random(data.dim)  
	allpoints = data.S 			
	distance = np.transpose(scp.distance.cdist(np.asmatrix(xnew), allpoints))
	d = np.amin(distance)
	if d < data.tolerance:
		output = None
	else: 
		output = xnew
	return output

def f_rbf(x, xev, lamb, gam, flag_ev): #predict function value with RBF
	yest = rbf_pred(np.asmatrix(x), xev, lamb, gam, flag_ev)
	return yest

def c_rbf(xin, x_01, lamb_01, gam_01, flag_01, eval_th, lb, ub): #evaluability threshold constraint
	x = np.asmatrix(xin)
	c = rbf_pred(np.asmatrix(x), x_01, lamb_01, gam_01, flag_01)
	c1 = -(-c+eval_th) #c(x) geq 0
	#lower and upper bounds
	c_lb = np.zeros(len(lb))
	c_ub = np.zeros(len(ub))
	cest = np.zeros(1+len(lb) + len(ub))
	cest[0] =c1
	for ii in range(len(lb)):
		c_lb[ii] = x[0,ii]-lb[ii]
		c_ub[ii] = ub[ii] - x[0,ii]
		cest[ii+1] = c_lb[ii]
		cest[len(c_lb)+ii+1]=c_ub[ii]
	return cest

def cycle_search(data,sample_stage):
	print('cycle sesrch')
	spar = np.isfinite(data.Y)
	ind = np.where(spar == True)[0]
	xev = data.S[ind,:]
    	yev = data.Y[ind]
	[lamb, gam] = rbf(xev,yev,data.flag_ev) #fit RBF to succesful points

	fail = np.where(np.isnan(data.Y))[0]
    	xnev = data.S[fail,:]
    	x_01 = data.S
    	f_01 = np.asmatrix(np.ones((data.S.shape[0],1)))
    	f_01[fail,:] = 0
    	#%fit linear RBF to {(x_01, f_01)} #predict evaluability
   	[lamb_01, gam_01] = rbf(x_01,f_01,data.flag_01)	
	
	regen = True
	while regen:
		print("Generating candidate points")
		rs = random.getstate()
		data.randstate = rs
		f = file('tempsol.data', 'w') 
    		p.dump(data, f)
    		f.close()
		cp = pert(data) #generate candidate points by perturbation
		np.savetxt('lamb.txt', lamb_01)
		np.savetxt('gam.txt', gam_01)
		np.savetxt('xxx.txt', x_01)
		c = rbf_pred(cp, x_01, lamb_01, gam_01, data.flag_01) #predict feasibility of candidate points
		keepID = np.where(c >= data.eval_th)[0] #keep candidates that are predicted to be successfully evaluated
		p_keep = cp[keepID,:]#remaining candidate points
		if len(keepID)==0: #n candidate point is predicted to be evaluable. keep all with positive value
			keepID = np.where(c >= 0)[0]
			p_keep = cp[keepID,:]
		if len(keepID)>0:
			regen = False 
	np.savetxt('xev.txt', xev)
	#%predict function value of remaining points
	y_pred = rbf_pred(p_keep, xev, lamb, gam, data.flag_ev)
	#compute scores for candidate points
	#%scale predicted objective function values to [0,1]    	
	min_ypred = np.amin(y_pred)
    	max_ypred = np.amax(y_pred)
    	if min_ypred == max_ypred:
        	scaled_ypred = np.ones((y_pred.shape[0], 1))
    	else:
        	scaled_ypred = (y_pred - min_ypred) / (max_ypred - min_ypred)
	d = np.transpose(scp.distance.cdist(p_keep, data.S))
	distv = np.asmatrix(np.amin(d, axis = 0)).T
	#%compute the distance between candidate points and all already sampled points

	#%scale distance values to [0,1]
	min_distv = np.amin(distv) 
 	max_distv = np.amax(distv)
	if min_distv == max_distv:
        	scaled_distv = np.ones((distv.shape[0], 1))
    	else:
        	scaled_distv = (distv - min_distv) / (max_distv - min_distv)
	cycID = sample_stage-2
	valueweight = data.w_cyc[cycID]
	
	#%compute weighted score for all candidates
	weighted_val = valueweight*scaled_ypred + (1 - valueweight)*scaled_distv
	#%assign inf scores to candidate points that are too close to already sampled
	#%points
	weighted_val[distv < data.tolerance] = np.inf
	
	selindex = np.argmin(weighted_val)
        x_new = np.array(p_keep[selindex, :]) #select new evaluation point as the one with lowest score
	
	if weighted_val[selindex] == np.inf: #%all candidates are too close to already evaluated points
    		x_new=None 
	xnew = np.ravel(x_new)
	return xnew


def pert(data): #function to create candidate points by perturbing the best point found so far
	cp_e = np.kron(np.ones((data.Ncands,1)), data.xbest)
        r=np.random.rand(data.Ncands,data.dim)
        a = r<data.P
        idx= np.where(np.sum(a,axis=1)==0)
        for ii in range(len(idx[0])):
            f = np.random.permutation(data.dim)
            a[idx[0][ii],f[0]] = True
        randnums = np.random.randn(data.Ncands, data.dim)
        randnums[a==False]=0
        pv = randnums*data.std
        new_pts = cp_e+pv
        for ii in range(data.dim):
            vec_ii = new_pts[:,ii]
            adj_l = np.where(vec_ii < data.xlow[ii])
            vec_ii[adj_l[0]] = data.xlow[ii] + (data.xlow[ii] - vec_ii[adj_l[0]])
            adj_u = np.where(vec_ii > data.xup[ii])
            vec_ii[adj_u[0]] = data.xup[ii] - (vec_ii[adj_u[0]]-data.xup[ii]) 
            stillout_u = np.where(vec_ii > data.xup[ii])
            vec_ii[stillout_u[0]] = data.xlow[ii]
            stillout_l = np.where(vec_ii < data.xlow[ii])
            vec_ii[stillout_l[0]] = data.xup[ii]
            new_pts[:,ii] = copy.copy(vec_ii)
	#create second set of candidates by random sampling 
	cp_r = np.asmatrix(np.random.uniform(0,1, [data.Ncands, data.dim]))
	candpoints = np.concatenate((copy.copy(new_pts), cp_r), axis = 0)
	return candpoints


def global_search(data): #global search = maximize minimum distance to already evaluated points
	print('global search')
	x_new = maximin_dist(data)
	distance = np.transpose(scp.distance.cdist(np.asmatrix(x_new), data.S))
	d = np.amin(distance)
	if d < data.tolerance:
		output = None
	else: 
		output = x_new
	return output

