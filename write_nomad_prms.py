import os
import subprocess
import numpy as np

def write_nomad_prms(bbeval, x0, data):
	testcase = 4
	dim = len(x0)

	#for x0, load the last point evaluated in main algorithm to avoid rounding error problem
	#can be skipped if small changes in x-vector will not lead to failed evaluations 
	with open('/home/julianem/hidden_constraints/allsamples_x.txt') as f:
		lines = f.readlines()
		last = lines[-1]
		for line in lines:
			if line is last:
				x = line
	p=map(float, x.split(','))
	x0=p[0:dim]
		
	#write NOMAD input file
	inp_str = "MAX_BB_EVAL " #assign max number of black box evals NOMAD is allowed to do
	set_str = ''.join((inp_str, str(bbeval)))
	aa = ''.join((set_str,'\n'))

	inp_str2="X0 ( "  #assign starting point
	for ii in range(len(x0)):
		inp_str2 = ''.join((inp_str2,str(x0[ii])))
		inp_str2 = ''.join((inp_str2,' '))
	bb = ''.join((inp_str2, ')\n'))

	inp_str3 = "DIMENSION " #assign dimension
	cc = ''.join((inp_str3, str(dim),'\n'))
	
	inp_str4a = "BB_EXE \"$python $/home/julianem/hidden_constraints/objectivewrap_for_nomad.py\"\n" #set black box execution file
	dd=inp_str4a 

	my_nomad_file = open('/home/julianem/hidden_constraints/basic_params.txt','r')
	nomad_basic = my_nomad_file.read()
	my_nomad_file.close()

	#write to nomad parameter input file
	my_new_input = open('/home/julianem/hidden_constraints/inputs_new.txt','w')
	my_new_input.write(aa)
	my_new_input.write(bb)
	my_new_input.write(cc)
	my_new_input.write(dd)
	my_new_input.write(nomad_basic)
	my_new_input.close()

	#call nomad -- needs to point to wherever your NOMAD is on the local machine
	p1 = subprocess.Popen("./nomad /home/julianem/hidden_constraints/inputs_new.txt",cwd='/home/julianem/ALS/python_codes/nomad.3.9.1/bin',stdout = subprocess.PIPE, shell = True).communicate()

	medata=np.loadtxt('/home/julianem/hidden_constraints/myhist.txt')
	if (medata.ndim == 1) and (np.isnan(medata[-1])): #in this case, rounding changed x0 just enough to make the point non-evaluable, so NOMAD crashes
		newS = None
		newY = None
	else:
		newS = np.asmatrix(medata[:,0:int(dim)])
		newY = np.asmatrix(medata[:,int(dim)])

	os.remove('/home/julianem/hidden_constraints/myhist.txt')
	return newS, newY #return the matrix of newly evaluated points and corresponding function values

